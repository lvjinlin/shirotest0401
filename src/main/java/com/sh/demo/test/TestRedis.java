package com.sh.demo.test;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class TestRedis {
    public static void main(String[] args) {
        //连接本地的 Redis 服务
//        Jedis jedis = new Jedis("127.0.0.1",6379);
//
//        //设置密码
////        jedis.auth("");
//        System.out.println("连接成功");
//        //查看服务是否运行
//        System.out.println("服务正在运行: "+jedis.ping());

        Set<HostAndPort> set=new HashSet<HostAndPort>();
        set.add(new HostAndPort("127.0.0.1",6379));
//        set.add(new HostAndPort("192.168.1.102",6380));

        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(100);
        config.setMaxIdle(10);
        config.setTestOnBorrow(true);



        JedisCluster jedisCluster=new JedisCluster(set,6000, 10, config);

        jedisCluster.set("123","432");
        System.out.println(jedisCluster.get("123"));


    }

}
